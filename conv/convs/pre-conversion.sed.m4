dnl Download and Rajkumar are useless headers
dnl and are littered all over the dictionary
/Download/d;
/Rajkumar/d;
s/¦/ु/g;
s/O\{/ई/g;
/\/m/	s//रू/g;

dnl adding m to end makes the letters have a tail
dnl So, we need special rules to convert them to unicode
dnl \[\]\[\'"\\F\|\{\}\] represents diacritics in Preeti
dnl so "k DIACTRITIC BLANK m" is transformed to unicode
dnl but the diactritics are placed as is to be converted
dnl later
/k([]['"\\F\|\{\}]*)[[:blank:]]*m/        s//फ\1/g;
/j([]['"\\F\|\{\}]*)[[:blank:]]*m/        s//क\1/g;
/e([]['"\\F\|\{\}]*)[[:blank:]]*m/        s//झ\1/g;
/Q([]['"\\F\|\{\}]*)[[:blank:]]*m/        s//क्त\1/g;
/q([]['"\\F\|\{\}]*)[[:blank:]]*m/        s//क्र\1/g;
/p([]['"\\F\|\{\}]*)[[:blank:]]*m/        s//ऊ\1/g;
