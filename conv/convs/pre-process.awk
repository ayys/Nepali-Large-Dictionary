# remove lines which start with Ctrl-L gg
# these lines are littered across the file
# and are used to show the page number.
# we don't need the page number so remove it
/^gg$/,/^[0-9]+$/    { next; }
               { print }
