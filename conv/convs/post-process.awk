# If the line looks like it is the start of a word
# and the previous line ended with purnaviram, there
# is a high possibility that this line is the start
# of a new word.
/^[^ \t—\(\)०-९][^ \t]+— +[^ ]+०/ {
    if (head && head ~ /। *$/)
        printf("\n");
    printf("%s ", $0);
    head=$0;
    next;
}
{ printf("%s ", $0); head=$0; }
