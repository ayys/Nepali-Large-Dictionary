#!/usr/bin/env bash

# Generate key map conversion stuff for sed
# $1 - Directory with Preeti stuff in it

ROOT=$(dirname $0)

cat $ROOT/special_chars_in_html.txt | $ROOT/../scripts/html2unicode > $ROOT/preeti_3_special_chars.txt

{
	# cat $ROOT/preeti_1_m_chars.txt
	cat	$ROOT/preeti_2_alphanumeric_chars.txt
	cat	$ROOT/preeti_4_punctuation_chars.txt
	cat	$ROOT/preeti_3_special_chars.txt        
} | awk '
/.+/ {
	 printf( "/%s/ s//%s/g;\n", $1, $2 );
 }
'
